# FFmpeg

FFmpeg with some improvements

* Support encode flv with hevc(h265) codec.
* Support decode flv with hevc(h265) codec.
* Support push rtmp stream with hevc(h265) codec.

Examples:

#### RTMP with H.265

```bash
ffmpeg -re -i video40.MP4 -vcodec libx265 -acodec aac -f flv rtmp://127.0.0.1/live/stream
```

#### FLV BOX (VOD)

```bash
ffmpeg -i video40.MP4 -t 10 -vcodec libx265 -acodec aac -f flv -y test.flv
```

#### Play

> Use ffplay address
